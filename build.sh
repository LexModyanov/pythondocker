#!/usr/bin/env bash

# This is the script that prepares the Python application to be run. It
# would normally be triggered from a derived docker image explicitly, or
# as a deferred ONBUILD action.

# Ensure that any failure within this script or a user provided script
# causes this script to fail immediately. This eliminates the need to
# check individual statuses for anything which is run and prematurely
# exit. Note that the feature of bash to exit in this way isn't
# foolproof. Ensure that you heed any advice in:
#
#   http://mywiki.wooledge.org/BashFAQ/105
#   http://fvue.nl/wiki/Bash:_Error_handling
#
# and use best practices to ensure that failures are always detected.
# Any user supplied scripts should also use this failure mode.

set -eo pipefail

# Set the umask to be '002' so that any files/directories created from
# this point are group writable. This does rely on any applications or
# installation scripts honouring the umask setting.

 umask 002

# Run any user supplied script to be run prior to installing application
# dependencies. This is to allow additional system packages to be
# installed that may be required by any Python modules which are being
# installed. The script must be executable in order to be run. It is not
# possible for this script to change the permissions so it is executable
# and then run it, due to some docker bug which results in the text file
# being busy. For more details see:
#
#   https://github.com/docker/docker/issues/9547


if [ -f pre-build.sh ]; then
    echo " -----> Running pre-build.sh"
    chmod a+x ./pre-build.sh
    sync
    ./pre-build.sh
fi

# Now run 'pip' to install any required Python packages based on the
# contents of the 'requirements.txt' file.

if [ -f requirements.txt ]; then
    echo " -----> Installing dependencies with pip"
    pip3 install -U --src=./tmp -r requirements.txt
fi

# Clean up any temporary files, including the results of checking out
# any source code repositories when doing a 'pip install' from a VCS.

rm -rf ./tmp
